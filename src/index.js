const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const app = express();

const PORT = 8080;
const { authRouter } = require('./controllers/authController');

app.use(morgan('tiny'));
app.use(express.json());

app.use('/api/auth', authRouter);

app.use((req, res, next) => {
  res.status(404).json({ message: 'Not found' });
});

const start = async () => {
  try {
    await mongoose.connect(
      'mongodb+srv://Mihail_DB_free:DSQrnn82@cluster0.s5swm.mongodb.net/UberDB?retryWrites=true&w=majority',
      { useNewUrlParser: true, useUnifiedTopology: true }
    );
    app.listen(PORT);
  } catch (err) {
    console.log('Error with connetcion', err);
  }
};

start();
