const mongoose = require('mongoose');

const Load = mongoose.model('Load', {
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    required: true
  },
  assigned_to: {
    type: String,
    require: true,
    default: null
  },
  status: {
    type: String,
    require: true,
    default: 'NEW',
    enum: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED']
  },
  state: {
    type: String,
    require: true,
    default: null,
    enum: [
      'En route to Pick Up',
      'Arrived to Pick Up',
      'En route to delivery',
      'Arrived to delivery'
    ]
  },
  name: {
    type: String,
    required: true
  },
  payload: {
    type: Number,
    require: true
  },
  pickup_address: {
    type: String,
    required: true
  },
  delivery_address: {
    type: String,
    required: true
  },
  dimensions: {
    width: {
      type: Number,
      require: true
    },
    length: {
      type: Number,
      require: true
    },
    height: {
      type: Number,
      require: true
    }
  },
  logs: [
    {
      message: {
        type: String,
        require: true
      },
      time: {
        type: Date,
        default: Date.now()
      }
    }
  ],
  created_date: {
    type: Date,
    default: Date.now()
  }
});

module.exports = {
  Load
};
