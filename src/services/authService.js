const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const { User } = require('../models/userModel');
const SECRET = 'secret';

const registration = async ({ email, password, role }) => {
  const user = new User({
    email,
    password: await bcrypt.hash(password, 10),
    role
  });
  try {
    await user.save();
  } catch (err) {
    throw new Error('You have already registered');
  }
};

const signIn = async ({ email, password }) => {
  const user = await User.findOne({ email });

  if (!user) {
    throw new Error('Invalid email or password');
  }

  if (!(await bcrypt.compare(password, user.password))) {
    throw new Error('Invalid email or password');
  }

  const token = jwt.sign(
    {
      _id: user._id,
      email: user.email,
      role: user.role
    },
    SECRET
  );
  return token;
};

const forgot_password = async ({ email }) => {
  const newPassword = '1234567890';
};

module.exports = {
  registration,
  signIn,
  forgot_password
};
