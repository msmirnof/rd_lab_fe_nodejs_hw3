const express = require('express');
const router = express.Router();

const {
  registration,
  signIn,
  forgot_password
} = require('../services/authService');
const { asyncWrapper } = require('../utils/apiUtils');
const {
  registrationValidator
} = require('../middlewares/validationMidlleware');

router.post(
  '/register',
  registrationValidator,
  asyncWrapper(async (req, res) => {
    const { email, password, role } = req.body;

    await registration({ email, password });

    res.json({ message: 'Profile created successfully' });
  })
);

router.post(
  '/login',
  asyncWrapper(async (req, res) => {
    const { email, password } = req.body;
    const token = await signIn({ email, password });

    await registration({ email, password });

    res.json({ token });
  })
);

router.post(
  '/forgot_password',
  asyncWrapper(async (req, res) => {
    const { email } = req.body;
    const token = await signIn({ email, password });

    await forgot_password({ email });

    res.json({ message: 'New password sent to your email address' });
  })
);

module.exports = {
  authRouter: router
};
